package com.example.mobilecalculatorandorid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView resultField;
    TextView stateField;
    double result;
    boolean operationComplete;
    String operation;
    double operand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultField = (TextView) findViewById(R.id.textView_output_result);
        stateField = (TextView) findViewById(R.id.textView_output_state);
        resultField.setText("0");
        stateField.setText("");
        result = 0.0;
        operationComplete = true;
        operation = "+";
        operand = 0.0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.action_settings :
                System.out.println("Настройки");
                return true;
            case R.id.open_settings:
                System.out.println("Журнал");
                Intent intent = new Intent(this, HistoryOperations.class);
                startActivity(intent);
                return true;
            case R.id.save_settings:
                System.out.println("Сохранить");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Todo remove console logs

    public void onNumberClick(View view) {
        String button = ((Button) view).getText().toString();
        System.out.println("Press on " + button);
        String resultString = this.resultField.getText().toString();
        String stateString = this.stateField.getText().toString();
        boolean flagClear = this.stateField.getText().toString().equals("") &&
                Double.parseDouble(resultString) == 0.0 && !resultString.contains(".");
        if (!this.operationComplete) {
            if (stateString.contains("=")) {
                this.clearState();
            } else {
                if (!Character.isDigit(stateString.charAt(stateString.length() - 1))) {
                    this.operation = Character.toString(stateString.charAt(stateString.length() - 1));
                }
            }
            operationComplete = true;
            flagClear = true;
        }

        if (!button.equals(".") && flagClear) {

            this.resultField.setText(button);
        } else {
            if (button.equals(".") && resultString.contains(".")) {
                System.out.println("Warning can not add dot");
            } else {
                resultString += button;
                this.resultField.setText(resultString);
            }
        }


    }

    public void onOperationClick(View view) {
        String button = ((Button) view).getText().toString();
        button = !button.equals("x^y")? button : "^";
        System.out.println("Press on " + button);
        String resultString = this.resultField.getText().toString();
        String stateString = this.stateField.getText().toString();

        switch (button) {
            case "AC":
                this.clearState();
                break;
            case "⌫":
                System.out.println("dell");
                if (this.operationComplete) {
                    if (resultString.length() > 1) {
                        resultString = resultString.substring(0, resultString.length() - 1);
                        this.resultField.setText(resultString);
                    } else {
                        this.resultField.setText("0");
                    }
                }
                break;
            case "±":
                if (this.operationComplete) {
                    if (resultString.charAt(0) == '-') {
                        resultString = resultString.substring(1);
                        this.resultField.setText(resultString);
                    } else {
                        resultString = "-" + resultString;
                        this.resultField.setText(resultString);
                    }
                }
                break;
            default:
                if (this.operationComplete) {
                    this.operand = Double.parseDouble(resultString);
                    this.operationComplete = false;
                    stateString = stateString + resultString + button;
                    this.stateField.setText(stateString);
                    this.count();
                } else {
                    if (stateString.contains("=")) {
                        if (button.equals("=")) {
                            changeFirstOperand();
                            count();
                        } else {
                            stateString = resultString + button;
                            this.stateField.setText(stateString);
                        }
                    } else {
                        stateString = stateString.substring(0, stateString.length() - 1) + button;
                        this.stateField.setText(stateString);
                    }
                }
        }
        System.out.println("Press on operation state is  " + stateString + " result " + resultString);
    }

    private void clearState() {
        resultField.setText("0");
        stateField.setText("");
        result = 0.0;
        operationComplete = true;
        operation = "+";
        operand = 0.0;

    }

    private void changeFirstOperand() {
        //Fixme please stringBuffer and method of solution
        String stateString = this.stateField.getText().toString();
        int i = stateString.length() - 1;
        if (stateString.charAt(i) == '=') {
            i--;
        }
        while (i > 0 && (stateString.charAt(i) == '.' || Character.isDigit(stateString.charAt(i)))) {
            i--;
        }
        if (i > 0) {
            stateString = Double.toString(this.result) + stateString.substring(i, stateString.length());
            System.out.println("change operand: " + stateString);
            this.stateField.setText(stateString);
        }
        System.out.println("Change first operand " + stateString);

    }

    private void count() {
        double result = 0.0;
        switch (this.operation) {
            case "+":
                result = this.result + this.operand;
                break;
            case "-":
                result = this.result - this.operand;
                break;
            case "x":
                result = this.result * this.operand;
                break;
            case "÷":
                if (this.operand != 0) {
                    result = this.result / this.operand;
                } else {
                    System.out.println("Деление на 0 невозможно");
                }
                break;
            case "^":
                result = Math.pow(this.result, this.operand);
                break;
        }
        System.out.println("count: " + this.result + " " + this.operation + " " + this.operand + " = " + result);
        this.result = result;
        this.resultField.setText(Double.toString(result));
    }

}