package com.example.mobilecalculatorandorid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class HistoryOperations extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_operations);

        String[] operations = {"1+2=3", "2x2=4", "123-3-10-10+100=200", "3-4-2-4=-7", "0.5x4+0.5=2.s"};

        ListView resulstList = (ListView) findViewById(R.id.resultsList);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, operations);
        resulstList.setAdapter(adapter);
    }
}